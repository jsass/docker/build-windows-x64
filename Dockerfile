FROM ubuntu:16.04
MAINTAINER "Tristan Lins" <tristan@lins.io>

COPY install-temurin.sh /bin

# Install required tools
RUN set -x \
    && apt-get update \
    && apt-get install -y apt-transport-https ca-certificates git curl wget \
                          build-essential libssl-dev mingw-w64 \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*

# Install cmake 3.21
RUN set -x \
    && DIR=$(mktemp -d) \
    && cd "$DIR" \
    && wget -nv --no-check-certificate http://www.cmake.org/files/v3.21/cmake-3.21.2.tar.gz -O cmake-3.21.2.tar.gz \
    && tar xf cmake-3.21.2.tar.gz \
    && cd cmake-3.21.2 \
    && ./configure \
    && make -j8 \
    && make install \
    && cd / \
    && rm -rf "$DIR"

# Install Java JDK 8
RUN set -x \
    && install-temurin.sh -f 8 -o linux -a x64 \
    && cd /usr/local/include \
    && ln -s /opt/java8/include/*.h /opt/java8/include/linux/*.h .

# Setup binaries and java home path
ENV PATH /opt/java8/bin:$PATH
ENV HOME /tmp
ENV JAVA_HOME /opt/java8

# Check java and gradle work properly
RUN set -x \
    && java -version

# Install make script
COPY make.sh /

# Run configuration
WORKDIR /jsass
CMD ["/make.sh"]
